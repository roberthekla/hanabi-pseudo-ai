include("hanabi.jl")
include("hanabi_player.jl")
include("hanabi_2d.jl")
include("hanabi_2d_game.jl")
include("hanabi_2d_player.jl")

players = [
    HanabiPlayer.construct_player_random_decision(),
    Hanabi2DPlayer.construct_player_no_dignity(),
    Hanabi2DPlayer.construct_player_no_dignity(),
]

Hanabi2DGame.run(
    Hanabi.create_random_initial_state(
        Hanabi2D.construct_default_cards((5, 5));
        num_players = length(players),
        hand_size = 5,
    ),
    players,
)
