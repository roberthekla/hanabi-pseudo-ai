module Hanabi2DWebIO

using ..Hanabi
using ..HanabiPlayer
using ..Hanabi2D
using ..Hanabi2DGame
using ..Hanabi2DPlayer
using Stipple
using StippleUI

function to_json_dict(state::Hanabi.State{2})
    Dict(
        "piled" => dropdims(
            sum(state.cards.piled; dims = 2);
            dims = 2,
        ),
        "hands" => [
            [
                hand_card !== nothing ? [Tuple(hand_card.card)...] : nothing
                for hand_card in hand
            ]
            for hand in eachrow(state.hands)
        ],
        "stacked" => reverse([
            [Tuple(card)...]
            for card in state.stack
        ]),
        "critical" => sort([
            [Tuple(card)...]
            for card in Hanabi.cards(state)
            if Hanabi2D.is_card_critical(state, card)
        ]),
        "garbage" => sort([
            [Tuple(card)...]
            for card in Hanabi.cards(state)
            if Hanabi2D.is_card_garbage(state, card)
        ]),
        "variables" => state.variables,
        "parameters" => Dict(
            "card_size" => Hanabi.card_size(state),
            "num_players" => Hanabi.num_players(state),
            "hand_size" => Hanabi.hand_size(state),
        )
    )
end

@reactive mutable struct HanabiReactiveModel{C, P, H} <: ReactiveModel where {C, P, H}
    state::R{Dict{String, Any}} = to_json_dict(
        Hanabi.create_random_initial_state(
            Hanabi2D.construct_default_cards(C);
            num_players = P,
            hand_size = H,
        )
    )

    players::R{Vector{String}} = repeat(["human"], P)

    card_probability::R{Matrix{Float64}} = Matrix{Float64}(undef, C)
    card_age::R{Int64} = 0

    colors::R{Vector{String}} = [
        "#99ff99",
        "#ff6699",
        "#ff6600",
        "#3366ff",
        "#996633",
        "#34eb89",
        "#eb3434",
    ]

    action_dialog::R{Bool} = false
    action_parameters::R{Dict} = Dict()
    action_type::R{String} = ""
    action_log::R{Vector{String}} = []

    process_player::R{Bool} = false
    auto_process_player::R{Bool} = false

    warning::R{Bool} = false
    warning_text::R{String} = ""
end

function process_action(
    state::Hanabi.State{2},
    reactive_model::HanabiReactiveModel,
    action::Tuple,
)
    @show action
    log = Hanabi2DGame.apply_action!(state, action)
    reactive_model.action_log[] = insert!(reactive_model.action_log[], 1, log)
    Hanabi.next_player!(state)
    reactive_model.state[] = to_json_dict(state)
    if reactive_model.auto_process_player[]
        reactive_model.process_player[] = true
    end
end

function construct_hanabi_observers(
    state::Hanabi.State{2},
    players::Vector{Union{HanabiPlayer.Player, Nothing}},
    reactive_model::HanabiReactiveModel,
)
    @assert length(players) == Hanabi.num_players(state)
    Observables.ObserverFunction[
        on(reactive_model.action_parameters) do parameters
            player = parameters["player"]
            position = parameters["position"]
            hand = state.hands[player, position]
            reactive_model.card_probability[] = hand.probability
            reactive_model.card_age[] = hand.age
        end,
        on(reactive_model.action_type) do type
            type != "" || return
            reactive_model.action_type[] = ""
            player = reactive_model.action_parameters[]["player"]
            position = reactive_model.action_parameters[]["position"]
            card = state.hands[player, position].card
            action = if type == "play"
                ("play", player, position)
            elseif type == "discard"
                ("discard", player, position)
            elseif type == "hint_dim_1"
                ("hint", state.variables[:active_player], player, 1, card[1])
            elseif type == "hint_dim_2"
                ("hint", state.variables[:active_player], player, 2, card[2])
            else
                throw(ArgumentError(type))
            end
            process_action(state, reactive_model, action)
        end,
        on(reactive_model.process_player) do process_player
            process_player || return
            reactive_model.process_player[] = false
            active_player = state.variables[:active_player]
            player = players[active_player]
            if player !== nothing
                action = player.on_turn(state, active_player)
                process_action(state, reactive_model, action)
            else
                reactive_model.warning_text[] = "Human intervention required!"
                reactive_model.warning[] = true
            end
        end,
    ]
end

function ui(model::HanabiReactiveModel)
    page(
        model,
        class = "container",
        title = "Hanabi",
        prepend = style(read("hanabi_2d.css", String)),
        read("hanabi_2d.html", String)
    )
end

end