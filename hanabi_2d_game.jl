module Hanabi2DGame

using ..Hanabi
using ..Hanabi2D

function apply_action!(
    state::Hanabi.State{2},
    action::Tuple,
) where {N}
    @assert action[1] in ["discard", "play", "hint"]

    if action[1] == "discard"
        log = "Player $(action[2]) discards card $(Tuple(state.hands[action[2], action[3]].card)).\n"
        Hanabi.player_discards_card!(
            state;
            player = action[2],
            position = action[3],
        )
        state.variables[:tokens][:hints][action[2]] += 1
    end

    if action[1] == "play"
        log = Hanabi2D.player_plays_card!(
            state;
            player = action[2],
            position = action[3],
        )
    end

    if action[1] == "hint"
        source_player = action[2]
        target_player = action[3]
        card_dim = action[4]
        card_value = action[5]

        log = (
            "Player $(source_player) hints player $(target_player) "
            * "card dim $(card_dim) with value $(card_value).\n"
        )

        Hanabi.player_hints_card!(
            state;
            source_player = source_player,
            target_player = target_player,
            card_dim = card_dim,
            card_value = card_value,
        )
        state.variables[:tokens][:hints][action[2]] -= 1

        actively_hinted_hand_cards = filter(
            hand_card -> (
                (
                    hand_card !== nothing
                ) && (
                    getindex(hand_card.card, card_dim) == card_value
                )
            ),
            state.hands[action[3], :],
        )

        newest = filter(
            hand_card -> (
                hand_card.age == minimum(
                    getfield.(actively_hinted_hand_cards, :age)
                )
            ),
            actively_hinted_hand_cards,
        )

        oldest = filter(
            hand_card -> (
                hand_card.age == maximum(
                    getfield.(actively_hinted_hand_cards, :age)
                )
            ),
            actively_hinted_hand_cards,
        )

        for hand_card in newest
            for card in Hanabi.cards(state)
                if hand_card.probability[card] > 0
                    hand_card.probability[card] += 1
                end
            end
        end

        pileable_cards = filter(
            card -> Hanabi2D.is_card_pileable(state, card),
            Hanabi.cards(state),
        )
        critical_cards = filter(
            card -> Hanabi2D.is_card_critical(state, card),
            Hanabi.cards(state),
        )

        for hand_card in newest
            for card in pileable_cards
                if hand_card.probability[card] > 0
                    hand_card.probability[card] += 1
                end
            end
        end

        for hand_card in oldest
            for card in critical_cards
                if hand_card.probability[card] > 0
                    hand_card.probability[card] += 1
                end
            end
        end
    end

    if action[1] in ["play", "discard"]
        for hand_card in state.hands[action[2], :]
            if hand_card !== nothing
                hand_card.age += 1
            end
        end

        if !isempty(state.stack)
            Hanabi.player_draws_card!(
                state;
                player = action[2],
                position = action[3],
            )
            log *= "Player $(action[2]) draws card $(Tuple(state.hands[action[2], action[3]].card)).\n"
        end

        state.hands[action[2], :] .= sort(
            state.hands[action[2], :],
            by = hand_card -> hand_card !== nothing ? hand_card.age : -1,
        )
    end

    log
end

function is_game_over(state::Hanabi.State{N}) where {N}
    (
        all(state.cards.piled .> 0)
    ) || (
        sum(state.variables[:tokens][:strikes]) >= 3
    ) || any([
        all(state.hands[player, :] .=== nothing)
        for player in 1:Hanabi.num_players(state)
    ])
end

function run(
    state::Hanabi.State{N},
    players::AbstractVector
) where {N}
    @assert length(players) == Hanabi.num_players(state)

    turn = 1

    while !is_game_over(state)
        active_player = state.variables[:active_player]

        action = players[state.variables[:active_player]].on_turn(
            state,
            state.variables[:active_player],
        )

        print("\n\nTurn $(turn) -- Player $(active_player)\n")
        @show action

        log = apply_action!(state, action)
        print(log)
        Hanabi.next_player!(state)

        turn += 1
        Hanabi2D.pretty_print(state)
        sleep(1)
    end

    score = sum(state.cards.piled .> 0)
    max_score = prod(size(state.cards.piled))

    print("GAME OVER!\n")
    print("Score = $(score) / $(max_score)\n")
end

end
