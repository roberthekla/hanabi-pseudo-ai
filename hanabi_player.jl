module HanabiPlayer

using ..Hanabi
using StatsBase

abstract type Player end

struct NoMemoryPlayer <: Player
    on_turn
    on_hint
end

function random_decision_on_turn(
    state::Hanabi.State{N}, player::I;
) where {N, I <: Int}
    sample([
        ("play", player, rand(1:Hanabi.hand_size(state))),
        ("discard", player, rand(1:Hanabi.hand_size(state))),
        begin
            card_dim = rand(1:N)
            (
                "hint",
                player,
                rand(1:Hanabi.num_players(state)),
                rand(1:N),
                rand(1:Hanabi.card_size(state, card_dim)),
            )
        end,
    ])
end

function random_decision_on_hint(
    state::Hanabi.State{N};
) where {N}

end

function construct_player_random_decision()
    NoMemoryPlayer(
        random_decision_on_turn,
        random_decision_on_hint,
    )
end

end
