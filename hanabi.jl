module Hanabi

using Random

struct CardState{N}
    stacked::Array{Int64, N}
    handed::Array{Int64, N}
    piled::Array{Int64, N}
    discarded::Array{Int64, N}
end

mutable struct HandCard{N}
    card::CartesianIndex{N}
    probability::Array{Float64, N}
    age::Int64
end

struct State{N}
    stack::Vector{CartesianIndex{N}}
    hands::Matrix{Union{HandCard{N}, Nothing}}
    cards::CardState{N}
    variables::Dict{Symbol, Any}
end

function card_size(state::State{N}) where {N}
    size(state.cards.stacked)
end

function card_size(state::State{N}, dim::I) where {N, I <: Int}
    size(state.cards.stacked, dim)
end

function cards(state::State{N}) where {N}
    CartesianIndices(card_size(state))
end

function num_players(state::State{N}) where {N}
    size(state.hands, 1)
end

function hand_size(state::State{N}) where {N}
    size(state.hands, 2)
end

# function cards(card_state::CardState{N}) where {N}
#     (
#         card_state.stacked
#         + card_state.handed
#         + card_state.piled
#         + card_state.discarded
#     )
# end

function construct_stack(cards::Array{Int64, N}) where {N}
    stack = CartesianIndex{N}[]
    for index in CartesianIndices(cards)
        push!(stack, Iterators.repeated(index, cards[index])...)
    end
    stack
end

function unknown_player_cards(
    state::State{N};
    player::I,
) where {N, I <: Int}
    unknown_cards = deepcopy(state.cards.stacked)
    for hand_card in state.hands[player, :]
        if hand_card !== nothing
            unknown_cards[hand_card.card] += 1
        end
    end
    unknown_cards
end

function update_player_hints!(
    state::State{N};
    player::I,
) where {N, I <: Int}
    unknown_cards = unknown_player_cards(state; player = player)
    for hand_card in state.hands[player, :]
        if hand_card !== nothing
            hand_card.hint .&= (unknown_cards .> 0)
        end
    end
    state
end

function update_hints!(state::State{N}) where {N}
    for player in 1:num_players(state)
        update_player_hints!(state; player = player)
    end
    state
end

function player_draws_card!(
    state::State{N};
    player::I,
    position::I,
) where {N, I <: Int}
    @assert !isempty(state.stack)
    @assert state.hands[player, position] === nothing

    card = pop!(state.stack)

    state.hands[player, position] = HandCard{N}(
        card,
        fill(1., card_size(state)),
        0,
    )
    state.cards.stacked[card] -= 1
    state.cards.handed[card] += 1

    @assert state.cards.stacked[card] >= 0

    state
end

function player_discards_card!(
    state::State{N};
    player::I,
    position::I,
) where {N, I <: Int}
    @assert state.hands[player, position] !== nothing

    card = state.hands[player, position].card

    state.hands[player, position] = nothing
    state.cards.handed[card] -= 1
    state.cards.discarded[card] += 1
    
    @assert state.cards.handed[card] >= 0

    state
end

function player_piles_card!(
    state::State{N};
    player::I,
    position::I,
) where {N, I <: Int}
    @assert state.hands[player, position] !== nothing
    
    card = state.hands[player, position].card

    state.hands[player, position] = nothing
    state.cards.handed[card] -= 1
    state.cards.piled[card] += 1
    
    @assert state.cards.handed[card] >= 0

    state
end

function player_hints_card!(
    state::State{N};
    source_player::I,
    target_player::I,
    card_dim::I,
    card_value::I,
) where {N, I <: Int}
    for hand_card in state.hands[target_player, :]
        if hand_card !== nothing
            for card in cards(state)
                hand_card.probability[card] *= (
                    hand_card.card[card_dim] == card_value
                    ? card[card_dim] == card_value
                    : card[card_dim] != card_value
                )
            end
        end
    end

    state.variables[:hints_provided][source_player] += 1
    state.variables[:hints_received][target_player] += 1
    
    state
end

function next_player!(state::State{N}) where {N}
    state.variables[:active_player] %= num_players(state)
    state.variables[:active_player] += 1
    state
end

function create_random_initial_state(
    cards::Array{Int64, N};
    num_players::I,
    hand_size::I,
) where {N, I <: Int}
    state = State{N}(
        shuffle(construct_stack(cards)),
        Matrix{Union{HandCard{N}, Nothing}}(
            nothing, num_players, hand_size
        ),
        CardState{N}(
            cards,
            zeros(Int64, size(cards)),
            zeros(Int64, size(cards)),
            zeros(Int64, size(cards)),
        ),
        Dict(
            :tokens => Dict(
                :hints => zeros(Int64, (num_players,)),
                :strikes => zeros(Int64, (num_players,)),
            ),
            :hints_received => zeros(Int64, (num_players,)),
            :hints_provided => zeros(Int64, (num_players,)),
            :active_player => 1,
        ),
    )

    for player in 1:num_players
        for position in 1:hand_size
            if !isempty(state.stack)
                player_draws_card!(
                    state;
                    player = player,
                    position = position,
                )
            end
        end
    end

    state
end

end
