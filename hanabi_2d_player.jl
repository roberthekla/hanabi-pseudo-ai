module Hanabi2DPlayer

using ..Hanabi
using ..Hanabi2D
using ..HanabiPlayer

function no_dignity_on_turn(
    state::Hanabi.State{N}, player::I;
) where {N, I <: Int}
    for (i, hand_card) in enumerate(state.hands[player, :])
        if hand_card !== nothing
            if Hanabi2D.is_card_pileable(state, hand_card.card)
                return ("play", player, i)
            end
        end
    end
    for (i, hand_card) in enumerate(state.hands[player, :])
        if hand_card !== nothing
            if Hanabi2D.is_card_garbage(state, hand_card.card)
                return ("discard", player, i)
            end
        end
    end
    for (i, hand_card) in enumerate(state.hands[player, :])
        if hand_card !== nothing
            if !Hanabi2D.is_card_critical(state, hand_card.card)
                return ("discard", player, i)
            end
        end
    end
    ("discard", argmin(getindex.(getfield.(state.hands[player, :], :card), 2)))
end

function servant_on_turn(
    state::Hanabi.State{N}, player::I;
) where {N, I <: Int}
    for (i, hand_card) in enumerate(state.hands[player, :])
        hand_card !== nothing || continue

        likely_cards = findall(>(0), hand_card.probability)

        if all(broadcast(card -> Hanabi2D.is_card_garbage(state, card), likely_cards))
            return ("discard", player, i)
        end
    end

    for (i, hand_card) in enumerate(state.hands[player, :])
        hand_card !== nothing || continue

        likely_cards = findall(>(0), hand_card.probability)

        if all(broadcast(card -> Hanabi2D.is_card_pileable(state, card), likely_cards))
            return ("play", player, i)
        end
    end

    for (i, hand_card) in enumerate(state.hands[player, :])
        hand_card !== nothing || continue

        most_likely_cards = findall(==(maximum(hand_card.probability)), hand_card.probability)

        if any(broadcast(card -> Hanabi2D.is_card_critical(state, card), most_likely_cards))
            continue
        end

        if any(broadcast(card -> Hanabi2D.is_card_pileable(state, card), most_likely_cards))
            return ("play", player, i)
        end
    end

    ("discard", player, rand(1:Hanabi.hand_size(state)))
end

function construct_player_no_dignity()
    HanabiPlayer.NoMemoryPlayer(
        no_dignity_on_turn,
        HanabiPlayer.random_decision_on_hint,
    )
end

function construct_player_servant()
    HanabiPlayer.NoMemoryPlayer(
        servant_on_turn,
        HanabiPlayer.random_decision_on_hint,
    )
end

end