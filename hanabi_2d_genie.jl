using Genie
Genie.down()
Genie.startup()

using Stipple

include("hanabi.jl")
include("hanabi_player.jl")
include("hanabi_2d.jl")
include("hanabi_2d_game.jl")
include("hanabi_2d_player.jl")
include("hanabi_2d_webio.jl")

state = nothing
players = nothing
reactive_model = nothing

route("/random") do
    player_types = String.(split(
        Genie.Requests.getpayload(:players, "human,random_decision,no_dignity"),
        ",",
    ))
    card_shape = (
        parse(Int64, Genie.Requests.getpayload(:colors, "5")),
        parse(Int64, Genie.Requests.getpayload(:numbers, "5")),
    )
    hand_size = parse(Int64, Genie.Requests.getpayload(:hand_size, "5"))

    @show card_shape
    @show player_types
    @show hand_size

    global state = Hanabi.create_random_initial_state(
        Hanabi2D.construct_default_cards(card_shape);
        num_players = length(player_types),
        hand_size = hand_size,
    )
    Hanabi2D.pretty_print(state)

    global players = Union{HanabiPlayer.Player, Nothing}[
        if player_type == "no_dignity"
            Hanabi2DPlayer.construct_player_no_dignity()
        elseif player_type == "random_decision"
            HanabiPlayer.construct_player_random_decision()
        elseif player_type == "servant"
            Hanabi2DPlayer.construct_player_servant()
        else
            @assert player_type == "human"
            nothing
        end
        for player_type in player_types
    ]

    global reactive_model = Stipple.init(
        Hanabi2DWebIO.HanabiReactiveModel{
            card_shape, length(player_types), hand_size
        }
    )

    on(reactive_model.isready) do isready
        isready || return
        Stipple.push!(reactive_model)
    end

    reactive_model.state[] = Hanabi2DWebIO.to_json_dict(state)
    reactive_model.players[] = player_types

    observers = Hanabi2DWebIO.construct_hanabi_observers(
        state, players, reactive_model
    )

    reactive_model |> Hanabi2DWebIO.ui |> html
end
