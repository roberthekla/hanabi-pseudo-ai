module Hanabi2D

using ..Hanabi
using DataFrames

function is_card_pileable(
    state::Hanabi.State{2},
    card::CartesianIndex{2},
)
    (
        all(state.cards.piled[card[1], 1:card[2] - 1] .> 0)
        && state.cards.piled[card] == 0
    )
end

function is_card_garbage(
    state::Hanabi.State{2},
    card::CartesianIndex{2},
)
    any([
        begin
            c = card + CartesianIndex(0, 1 - i)
            (
                state.cards.stacked[c]
                + state.cards.handed[c]
                + state.cards.piled[c]
            )
        end == 0
        for i in 1:card[2]
    ]) || (
        state.cards.piled[card] > 0
    )
end

function is_card_critical(
    state::Hanabi.State{2},
    card::CartesianIndex{2},
)
    (
        state.cards.piled[card] == 0
    ) && (
        state.cards.handed[card]
        + state.cards.stacked[card]
        == 1
    ) && !is_card_garbage(state, card)
end

function construct_default_cards(shape::NTuple{2, Int64})
    cards = Array{Int64, 2}(undef, shape...)
    cards[:, 1] .= 3
    cards[:, 2:end - 1] .= 2
    cards[:, end] .= 1
    cards
end

function player_plays_card!(
    state::Hanabi.State{2};
    player::I,
    position::I,
) where {I <: Int}
    @assert state.hands[player, position] !== nothing

    card = state.hands[player, position].card
    log = "Player $(player) plays card $(Tuple(card)) and "

    if is_card_pileable(state, card)
        Hanabi.player_piles_card!(
            state;
            player = player,
            position = position,
        )
        log *= "piles it.\n"
    else
        Hanabi.player_discards_card!(
            state;
            player = player,
            position = position,
        )
        state.variables[:tokens][:strikes][player] += 1
        log *= "has to discard it, receiving a strike.\n"
    end

    log
end

function pretty_print(state::Hanabi.State{2})
    print("\nGAME STATE\n\n")
    print("$(state.variables[:tokens][:hints]) hint   tokens\n")
    print("$(state.variables[:tokens][:strikes]) strike tokens\n")
    print("\n")
    display(DataFrame(
        stacked = dropdims(sum(state.cards.stacked; dims = 2); dims = 2),
        handed = dropdims(sum(state.cards.handed; dims = 2); dims = 2),
        piled = dropdims(sum(state.cards.piled; dims = 2); dims = 2),
        discarded = dropdims(sum(state.cards.discarded; dims = 2); dims = 2),
    ))
    print("\n")

    for player in 1:size(state.hands, 1)
        display(DataFrame(
            card = broadcast(
                hand_card -> begin
                    if hand_card !== nothing
                        Tuple(hand_card.card)
                    else
                        nothing
                    end
                end,
                state.hands[player, :],
            ),
            colors = broadcast(
                hand_card -> begin
                    if hand_card !== nothing
                        ss = sum(hand_card.probability; dims = 2)[:, 1]
                        [i for (i, s) in enumerate(ss) if s > 0]
                    else
                        nothing
                    end
                end,
                state.hands[player, :],
            ),
            numbers = broadcast(
                hand_card -> begin
                    if hand_card !== nothing
                        ss = sum(hand_card.probability; dims = 1)[1, :]
                        [i for (i, s) in enumerate(ss) if s > 0]
                    else
                        nothing
                    end
                end,
                state.hands[player, :],
            )
        ))
        print("\n")
    end
end

end